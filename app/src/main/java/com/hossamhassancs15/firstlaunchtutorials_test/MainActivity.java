package com.hossamhassancs15.firstlaunchtutorials_test;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    public static boolean firstTime=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        try {
            firstTime = prefs.getBoolean("Time", false);
        }catch (Exception e){
            firstTime=false;
        }
        if (!firstTime) {

            Intent i=new Intent(this,FirstLaunchActivity.class);
            startActivity(i);
            finish();
            return;
        }
        Toast.makeText(MainActivity.this, "Welcome to main Activity ", Toast.LENGTH_SHORT).show();



    }








}
